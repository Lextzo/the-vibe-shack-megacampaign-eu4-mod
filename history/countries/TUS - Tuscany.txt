government = republic
add_government_reform = oligarchy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = austrian
capital = 116
1444.9.11= {
	monarch = {
		name = "Welf"
		adm = 2
		dip = 4
		mil = 2
		birth_date = 1405.8.25
		religion = catholic
		culture = austrian
	}
	add_ruler_personality = righteous_personality
	add_ruler_personality = strict_personality
}
1444.9.11= {
	add_prestige = 15
	add_treasury = 54
}
