government = monarchy
add_government_reform = grand_duchy_reform
government_rank = 1
technology_group = eastern
religion = catholic
primary_culture = finnish
capital = 309
1444.9.11= {
	monarch = {
		name = "Kaleva"
		dynasty = "of Sortavala"
		adm = 2
		dip = 4
		mil = 2
		birth_date = 1397.7.20
		religion = catholic
		culture = finnish
	}
	add_ruler_personality = fertile_personality
	add_ruler_personality = zealot_personality
	queen = {
		name = "Aino"
		dynasty = "Sepp"
		adm = 2
		dip = 2
		mil = 3
		country_of_origin = KRL
		birth_date = 1387.12.5
		death_date = 1447.12.5
		female = yes
		religion = catholic
		culture = finnish
	}
	add_queen_personality = craven_personality
	add_queen_personality = intricate_web_weaver_personality
	heir = {
		name = "Marja"
		monarch_name = "Marja"
		dynasty = "Sepp"
		adm = 3
		dip = 5
		mil = 3
		birth_date = 1423.12.16
		death_date = 1488.12.16
		female = yes
		claim = 89
		religion = catholic
		culture = finnish
	}
	add_heir_personality = strict_personality
	add_heir_personality = tactical_genius_personality
}
1444.9.11= {
	add_prestige = 48
	add_treasury = 55
}
