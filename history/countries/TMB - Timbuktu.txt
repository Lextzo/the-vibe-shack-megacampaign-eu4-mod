government = monarchy
add_government_reform = elective_monarchy
government_rank = 1
technology_group = western
religion = fraticelli
primary_culture = cosmopolitan_french
capital = 1132
1444.9.11= {
	monarch = {
		name = "Anne"
		dynasty = "von Metz"
		adm = 2
		dip = 3
		mil = 2
		birth_date = 1412.1.12
		female = yes
		religion = catholic
		culture = cosmopolitan_french
	}
	add_ruler_personality = strict_personality
	add_ruler_personality = zealot_personality
	queen = {
		name = "Reinhard"
		dynasty = "Otakeren"
		adm = 2
		dip = 3
		mil = 3
		country_of_origin = TMB
		birth_date = 1419.6.19
		death_date = 1479.6.19
		religion = catholic
		culture = alsatian
	}
	add_queen_personality = silver_tongue_personality
	add_queen_personality = tactical_genius_personality
}
1444.9.11= {
	add_prestige = 44
	add_treasury = 0
}
