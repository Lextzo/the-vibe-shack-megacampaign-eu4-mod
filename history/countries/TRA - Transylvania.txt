government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = hungarian
capital = 4128
1444.9.11= {
	monarch = {
		name = "Heinrich"
		dynasty = "de Savoie"
		adm = 5
		dip = 4
		mil = 4
		birth_date = 1411.6.26
		religion = catholic
		culture = austrian
	}
	add_ruler_personality = conqueror_personality
	add_ruler_personality = intricate_web_weaver_personality
	queen = {
		name = "Irmele"
		dynasty = "von Isenberg"
		adm = 4
		dip = 3
		mil = 3
		country_of_origin = TRA
		birth_date = 1426.10.2
		death_date = 1486.10.2
		female = yes
		religion = catholic
		culture = alsatian
	}
	add_queen_personality = lawgiver_personality
	add_queen_personality = tactical_genius_personality
	heir = {
		name = "Engeltraud"
		monarch_name = "Engeltraud"
		dynasty = "de Savoie"
		adm = 2
		dip = 0
		mil = 2
		birth_date = 1440.2.5
		death_date = 1505.2.5
		female = yes
		claim = 89
		religion = catholic
		culture = alsatian
	}
	add_heir_personality = well_connected_personality
	add_heir_personality = zealot_personality
}
1444.9.11= {
	add_prestige = 72
	add_treasury = 69
}
