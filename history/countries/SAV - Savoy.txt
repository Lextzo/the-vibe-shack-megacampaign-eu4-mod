government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = swiss
capital = 205
1444.9.11= {
	monarch = {
		name = "Baldomar"
		dynasty = "von Wittelsbach"
		adm = 2
		dip = 3
		mil = 3
		birth_date = 1419.6.4
		religion = catholic
		culture = swiss
	}
	add_ruler_personality = conqueror_personality
	add_ruler_personality = sinner_personality
	queen = {
		name = "Agn�s"
		dynasty = "de Ch�tellerault"
		adm = 4
		dip = 5
		mil = 3
		country_of_origin = SAV
		birth_date = 1421.6.12
		death_date = 1481.6.12
		female = yes
		religion = catholic
		culture = burgundian
	}
	add_queen_personality = naive_personality
	add_queen_personality = tolerant_personality
}
1444.9.11= {
	add_prestige = 51
	add_treasury = 62
}
