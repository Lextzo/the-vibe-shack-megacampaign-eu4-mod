government = republic
add_government_reform = free_city
government_rank = 1
technology_group = western
religion = catholic
primary_culture = swedish
capital = 25
mercantilism = 25
1444.9.11= {
	monarch = {
		name = "Birgitta"
		dynasty = "Mac Gilla-P�traic"
		adm = 3
		dip = 2
		mil = 3
		birth_date = 1427.1.5
		female = yes
		religion = catholic
		culture = swedish
	}
	add_ruler_personality = midas_touched_personality
	add_ruler_personality = tactical_genius_personality
	queen = {
		name = "Sakoura"
		dynasty = "Saarbr�cken"
		adm = 4
		dip = 3
		mil = 3
		country_of_origin = GOT
		birth_date = 1427.1.23
		death_date = 1487.1.23
		religion = catholic
		culture = alsatian
	}
	add_queen_personality = loose_lips_personality
	add_queen_personality = righteous_personality
}
1444.9.11= {
	add_prestige = 22
	add_treasury = 75
}
