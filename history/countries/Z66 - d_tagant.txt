government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = german
capital = 1129
1444.9.11= {
	monarch = {
		name = "(Regency Council)"
		adm = 1
		dip = 1
		mil = 1
		birth_date = 1.1.1
		regent = yes
		religion = catholic
		culture = german
	}
	heir = {
		name = "Adalhard"
		dynasty = "von Mansfeld"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 1437.1.19
		death_date = 1502.1.19
		claim = 89
		religion = catholic
		culture = german
	}
}
1444.9.11= {
	add_prestige = -23
	add_treasury = 65
}
