government = monarchy
add_government_reform = feudalism_reform
government_rank = 2
technology_group = western
religion = catholic
primary_culture = scottish
capital = 251
1444.9.11= {
	monarch = {
		name = "Aldonza"
		dynasty = "Mac Gilla-P�traic"
		adm = 3
		dip = 2
		mil = 3
		birth_date = 1397.1.19
		female = yes
		religion = catholic
		culture = castillian
	}
	add_ruler_personality = silver_tongue_personality
	add_ruler_personality = zealot_personality
	queen = {
		name = "Rodrigo"
		dynasty = "Mac Gilla-P�traic"
		adm = 2
		dip = 3
		mil = 1
		country_of_origin = SCO
		birth_date = 1385.12.10
		death_date = 1445.12.10
		religion = catholic
		culture = galician
	}
	add_queen_personality = fierce_negotiator_personality
	add_queen_personality = tactical_genius_personality
	heir = {
		name = "Rodrigo"
		monarch_name = "Rodrigo"
		dynasty = "Mac Gilla-P�traic"
		adm = 6
		dip = 6
		mil = 6
		birth_date = 1422.9.21
		death_date = 1487.9.21
		claim = 89
		religion = catholic
		culture = galician
	}
	add_heir_personality = intricate_web_weaver_personality
	add_heir_personality = lawgiver_personality
}
1444.9.11= {
	add_prestige = 58
	add_treasury = 60
}
