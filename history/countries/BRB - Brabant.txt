government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = wallonian
capital = 92
1444.9.11= {
	monarch = {
		name = "Stefan"
		dynasty = "Vr�ovic"
		adm = 3
		dip = 4
		mil = 4
		birth_date = 1394.12.28
		religion = catholic
		culture = prussian
	}
	add_ruler_personality = embezzler_personality
	add_ruler_personality = midas_touched_personality
	queen = {
		name = "Cecilie"
		dynasty = "von Lechsgm�nd"
		adm = 3
		dip = 5
		mil = 3
		country_of_origin = BRB
		birth_date = 1418.5.16
		death_date = 1478.5.16
		female = yes
		religion = catholic
		culture = alsatian
	}
	add_queen_personality = expansionist_personality
	add_queen_personality = silver_tongue_personality
	heir = {
		name = "Adam"
		monarch_name = "Adam"
		dynasty = "Vr�ovic"
		adm = 0
		dip = 0
		mil = 1
		birth_date = 1442.12.2
		death_date = 1507.12.2
		claim = 89
		religion = catholic
		culture = alsatian
	}
	add_heir_personality = well_connected_personality
	add_heir_personality = zealot_personality
}
1444.9.11= {
	add_prestige = 26
	add_treasury = 51
}
