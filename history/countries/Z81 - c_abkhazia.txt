government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
technology_group = eastern
religion = orthodox
primary_culture = georgian
capital = 1856
fixed_capital = 1856
1444.9.11= {
	monarch = {
		name = "(Regency Council)"
		adm = 4
		dip = 3
		mil = 3
		birth_date = 1.1.1
		regent = yes
		religion = orthodox
		culture = georgian
	}
	heir = {
		name = "Gagiki"
		dynasty = "Jaqeli"
		adm = 6
		dip = 5
		mil = 5
		birth_date = 1428.11.15
		death_date = 1493.11.15
		claim = 89
		religion = orthodox
		culture = georgian
	}
}
1444.9.11= {
	add_prestige = -50
	add_treasury = 60
}
