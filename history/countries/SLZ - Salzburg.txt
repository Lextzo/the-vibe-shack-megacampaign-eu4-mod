government = theocracy
add_government_reform = leading_clergy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = bavarian
capital = 76
elector = yes
1444.9.11= {
	monarch = {
		name = "Folkhard"
		adm = 5
		dip = 3
		mil = 4
		birth_date = 1395.10.10
		religion = catholic
		culture = bavarian
	}
	add_ruler_personality = kind_hearted_personality
	add_ruler_personality = lawgiver_personality
}
1444.9.11= {
	add_prestige = 42
	add_treasury = 72
}
