government = monarchy
add_government_reform = rajput_kingdom
government_rank = 1
technology_group = indian
religion = zun_pagan_reformed
primary_culture = sindhi
capital = 2049
1444.9.11= {
	monarch = {
		name = "Bhungar Rao"
		dynasty = "Khijjingid"
		adm = 3
		dip = 4
		mil = 5
		birth_date = 1424.12.27
		religion = zun_pagan_reformed
		culture = sindhi
	}
	add_ruler_personality = free_thinker_personality
	add_ruler_personality = lawgiver_personality
}
1444.9.11= {
	add_prestige = 23
	add_treasury = 78
}
