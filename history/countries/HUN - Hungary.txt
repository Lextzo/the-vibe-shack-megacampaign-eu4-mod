government = monarchy
add_government_reform = autocracy_reform
government_rank = 2
technology_group = eastern
religion = catholic
primary_culture = hungarian
capital = 153
1444.9.11= {
	monarch = {
		name = "Elek"
		dynasty = "J�k"
		adm = 2
		dip = 3
		mil = 1
		birth_date = 1421.2.15
		religion = catholic
		culture = hungarian
	}
	add_ruler_personality = tactical_genius_personality
	add_ruler_personality = tolerant_personality
	queen = {
		name = "Oda"
		dynasty = "de Savoie"
		adm = 3
		dip = 4
		mil = 4
		country_of_origin = Z53
		birth_date = 1418.4.24
		death_date = 1478.4.24
		female = yes
		religion = catholic
		culture = alsatian
	}
	add_queen_personality = strict_personality
	add_queen_personality = tactical_genius_personality
}
1444.9.11= {
	add_prestige = 65
	add_treasury = 68
}
