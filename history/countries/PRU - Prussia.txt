government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = prussian
capital = 41
1444.9.11= {
	monarch = {
		name = "Gotzelo II"
		dynasty = "von Lechsgmünd"
		adm = 3
		dip = 2
		mil = 4
		birth_date = 1394.11.16
		religion = catholic
		culture = prussian
	}
	add_ruler_personality = fierce_negotiator_personality
	add_ruler_personality = tolerant_personality
	queen = {
		name = "Gudrun"
		dynasty = "von Goseck"
		adm = 5
		dip = 3
		mil = 5
		country_of_origin = PRU
		birth_date = 1394.12.5
		death_date = 1454.12.5
		female = yes
		religion = catholic
		culture = alsatian
	}
	add_queen_personality = infertile_personality
	add_queen_personality = well_connected_personality
	heir = {
		name = "Heribert"
		monarch_name = "Heribert II"
		dynasty = "von Lechsgmünd"
		adm = 3
		dip = 5
		mil = 4
		birth_date = 1423.8.28
		death_date = 1488.8.28
		claim = 89
		religion = catholic
		culture = alsatian
	}
	add_heir_personality = infertile_personality
	add_heir_personality = sinner_personality
}
1444.9.11= {
	add_prestige = 57
	add_treasury = 58
}
