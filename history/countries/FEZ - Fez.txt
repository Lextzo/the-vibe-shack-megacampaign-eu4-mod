government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = moroccan
capital = 342
1444.9.11= {
	monarch = {
		name = "Hemma"
		dynasty = "Premyslid"
		adm = 6
		dip = 3
		mil = 5
		birth_date = 1389.12.7
		female = yes
		religion = catholic
		culture = german
	}
	add_ruler_personality = careful_personality
	add_ruler_personality = midas_touched_personality
	queen = {
		name = "Ehrenfried"
		dynasty = "Premyslid"
		adm = 5
		dip = 3
		mil = 3
		country_of_origin = FEZ
		birth_date = 1415.10.4
		death_date = 1475.10.4
		religion = catholic
		culture = alsatian
	}
	add_queen_personality = bold_fighter_personality
	add_queen_personality = fierce_negotiator_personality
	heir = {
		name = "Yuba"
		monarch_name = "Yuba"
		dynasty = "de L�vis"
		adm = 4
		dip = 5
		mil = 6
		birth_date = 1412.10.2
		death_date = 1477.10.2
		claim = 89
		religion = catholic
		culture = tuareg
	}
	add_heir_personality = infertile_personality
	add_heir_personality = just_personality
}
1444.9.11= {
	add_prestige = 61
	add_treasury = 54
}
