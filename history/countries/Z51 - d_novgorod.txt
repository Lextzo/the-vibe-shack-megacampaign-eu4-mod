government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = danish
capital = 1753
1444.9.11= {
	monarch = {
		name = "Karl"
		dynasty = "af Ch�teau-Renault"
		adm = 3
		dip = 2
		mil = 2
		birth_date = 1413.8.10
		religion = catholic
		culture = danish
	}
	add_ruler_personality = obsessive_perfectionist_personality
	add_ruler_personality = tactical_genius_personality
	queen = {
		name = "Maija"
		dynasty = "Kangas"
		adm = 3
		dip = 2
		mil = 2
		country_of_origin = Z51
		birth_date = 1415.11.7
		death_date = 1475.11.7
		female = yes
		religion = catholic
		culture = finnish
	}
	add_queen_personality = industrious_personality
	add_queen_personality = midas_touched_personality
	heir = {
		name = "Gotfred"
		monarch_name = "Gotfred II"
		dynasty = "af Ch�teau-Renault"
		adm = 0
		dip = 0
		mil = 0
		birth_date = 1444.8.2
		death_date = 1509.8.2
		claim = 89
		religion = catholic
		culture = danish
	}
	add_heir_personality = well_connected_personality
	add_heir_personality = zealot_personality
}
1444.9.11= {
	add_prestige = 30
	add_treasury = 36
}
