government = theocracy
add_government_reform = papacy_reform
government_rank = 2
technology_group = western
religion = catholic
primary_culture = umbrian
capital = 109
1444.9.11= {
	monarch = {
		name = "Callistus IV"
		dynasty = "von Venis"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 1384.12.14
		religion = catholic
		culture = umbrian
	}
	add_ruler_personality = pious_personality
	add_ruler_personality = zealot_personality
}
1444.9.11= {
	add_prestige = 57
	add_treasury = 78
}
