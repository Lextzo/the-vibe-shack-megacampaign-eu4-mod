government = monarchy
add_government_reform = binhilde_government
government_rank = 2
technology_group = indian
religion = zun_pagan_reformed
primary_culture = sindhi
capital = 506
1444.9.11= {
	monarch = {
		name = "Shakuntala"
		dynasty = "Binhildesylid"
		adm = 3
		dip = 4
		mil = 2
		birth_date = 1428.1.24
		female = yes
		religion = zun_pagan_reformed
		culture = sindhi
	}
	add_ruler_personality = just_personality
	add_ruler_personality = strict_personality
}
1444.9.11= {
	add_prestige = 71
	add_treasury = 58
}
