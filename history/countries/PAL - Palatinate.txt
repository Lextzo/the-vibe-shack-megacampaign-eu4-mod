government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
technology_group = western
religion = catholic
primary_culture = hessian
capital = 78
elector = yes
1444.9.11= {
	monarch = {
		name = "Hartmann"
		dynasty = "von Zelking"
		adm = 3
		dip = 4
		mil = 4
		birth_date = 1390.8.26
		religion = catholic
		culture = hessian
	}
	add_ruler_personality = intricate_web_weaver_personality
	add_ruler_personality = secretive_personality
	queen = {
		name = "Guraya"
		dynasty = "Hafizid"
		adm = 2
		dip = 3
		mil = 2
		country_of_origin = PAL
		birth_date = 1406.5.10
		death_date = 1466.5.10
		female = yes
		religion = catholic
		culture = tuareg
	}
	add_queen_personality = scholar_personality
	add_queen_personality = zealot_personality
	heir = {
		name = "Ute"
		monarch_name = "Ute"
		dynasty = "von Zelking"
		adm = 4
		dip = 5
		mil = 6
		birth_date = 1418.1.2
		death_date = 1483.1.2
		female = yes
		claim = 89
		religion = catholic
		culture = alsatian
	}
	add_heir_personality = charismatic_negotiator_personality
	add_heir_personality = strict_personality
}
1444.9.11= {
	add_prestige = 79
	add_treasury = 62
}
