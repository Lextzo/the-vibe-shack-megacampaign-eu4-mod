owner = STY
controller = STY
capital = "Meldorf"
is_city = yes
culture = danish
religion = catholic
trade_goods = grain
hre = yes
base_tax = 4
base_production = 3
base_manpower = 2
add_core = STY
add_permanent_claim = DAN
add_permanent_claim = SHL
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
