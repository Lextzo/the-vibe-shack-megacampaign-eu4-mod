owner = KSH
controller = KSH
capital = "Kangra"
is_city = yes
culture = kashmiri
religion = hinduism
trade_goods = cotton
base_tax = 4
base_production = 5
base_manpower = 2
add_core = KSH
add_core = NPL
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
