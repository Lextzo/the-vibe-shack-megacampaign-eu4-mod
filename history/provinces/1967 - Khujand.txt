owner = TRS
controller = TRS
capital = "Khujand"
is_city = yes
culture = sindhi
religion = zun_pagan_reformed
trade_goods = silk
base_tax = 1
base_production = 1
base_manpower = 1
add_core = TRS
add_permanent_claim = KOK
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
