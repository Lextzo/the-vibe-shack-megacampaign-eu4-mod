owner = MER
controller = MER
capital = "Kotharia"
is_city = yes
culture = sindhi
religion = buddhism
trade_goods = gold
base_tax = 5
base_production = 5
base_manpower = 4
add_core = MER
add_permanent_claim = Z12
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
