owner = AUV
controller = AUV
capital = "Clermont"
is_city = yes
culture = occitain
religion = catholic
trade_goods = iron
base_tax = 5
base_production = 3
base_manpower = 3
hre = yes
add_core = AUV
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
latent_trade_goods = {
	coal
}
