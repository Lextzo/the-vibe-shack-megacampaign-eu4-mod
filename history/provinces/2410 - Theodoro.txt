owner = Z32
controller = Z32
capital = "Mangul"
is_city = yes
culture = goths
religion = catholic
trade_goods = wine
base_tax = 5
base_production = 7
base_manpower = 4
add_core = FEO
add_core = Z32
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
