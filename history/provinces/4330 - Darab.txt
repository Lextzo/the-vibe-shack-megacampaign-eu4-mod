owner = TIM
controller = TIM
capital = "Darab"
is_city = yes
culture = turkish
religion = sunni
trade_goods = spices
base_tax = 7
base_production = 3
base_manpower = 2
add_core = TIM
add_core = TUR
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
