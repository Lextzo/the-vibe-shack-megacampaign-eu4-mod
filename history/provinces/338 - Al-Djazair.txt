owner = ALG
controller = ALG
capital = "Al-Djazair"
is_city = yes
culture = algerian
religion = catholic
trade_goods = grain
# hre = yes
base_tax = 11
base_production = 3
base_manpower = 5
extra_cost = 8
center_of_trade = 1
add_core = ALG
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
