owner = Z00
controller = Z00
capital = "Smederevo"
is_city = yes
culture = serbian
religion = paulician
trade_goods = grain
fort_15th = yes
base_tax = 5
base_production = 6
base_manpower = 3
add_core = SER
add_core = Z00
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
