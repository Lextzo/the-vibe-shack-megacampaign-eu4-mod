owner = TIB
controller = TIB
capital = "Chittagong"
is_city = yes
culture = bengali
religion = vajrayana
trade_goods = cotton
base_tax = 3
base_production = 6
base_manpower = 3
extra_cost = 24
center_of_trade = 3
add_core = TIB
add_permanent_claim = BNG
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
