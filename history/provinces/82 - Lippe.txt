owner = BOH
controller = BOH
capital = "Arnsberg"
is_city = yes
culture = hannoverian
religion = catholic
trade_goods = wool
hre = yes
base_tax = 7
base_production = 3
base_manpower = 4
add_core = BOH
add_core = WES
add_permanent_claim = PAP
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
latent_trade_goods = {
	coal
}
