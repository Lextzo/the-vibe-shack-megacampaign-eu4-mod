owner = MAD
controller = MAD
capital = "Thanjavur"
is_city = yes
culture = tamil
religion = hinduism
trade_goods = cloth
fort_15th = yes
base_tax = 9
base_production = 11
base_manpower = 5
extra_cost = 16
center_of_trade = 2
add_core = MAD
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
