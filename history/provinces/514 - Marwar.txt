owner = SND
controller = SND
capital = "Mandore"
is_city = yes
culture = sindhi
religion = zun_pagan_reformed
trade_goods = cotton
base_tax = 7
base_production = 1
base_manpower = 8
add_core = SND
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
