owner = TIM
controller = TIM
capital = "Sila"
is_city = yes
culture = turkish
religion = sunni
trade_goods = fish
base_tax = 6
base_production = 4
base_manpower = 6
add_core = TIM
add_core = TUR
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
