owner = SND
controller = SND
capital = "Sehwan"
is_city = yes
culture = turkish
religion = zun_pagan_reformed
trade_goods = silk
base_tax = 5
base_production = 3
base_manpower = 2
add_core = SND
add_core = TUR
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
