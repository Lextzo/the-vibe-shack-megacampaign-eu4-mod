owner = Z32
controller = Z32
capital = "Kaffa"
is_city = yes
culture = goths
religion = catholic
trade_goods = wine
base_tax = 5
base_production = 6
base_manpower = 4
extra_cost = 16
center_of_trade = 2
add_core = FEO
add_core = Z32
discovered_by = eastern
discovered_by = indian
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman
discovered_by = western
