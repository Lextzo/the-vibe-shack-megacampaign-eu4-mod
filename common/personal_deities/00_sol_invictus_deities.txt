# Do not change tags in here without changing every other reference to them.
# If adding new dieties, make sure they are uniquely named.

binhilde = #1
{
	legitimacy = 1
	horde_unity = 1
	range = 0.5
	
	allow = { religion = zun_pagan_reformed }
	sprite = 55 #Because no GFX
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0.5
			personality = ai_militarist
		}	
		modifier = {
			factor = 2
			personality = ai_colonialist
		}
	}
}

helios = #2
{
	global_manpower_modifier = 0.1
	production_efficiency = 0.1
	
	allow = { religion = zun_pagan_reformed }
	sprite = 55 #Because no GFX
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_balanced
		}	
		modifier = {
			factor = 0.5
			personality = ai_colonialist
		}
	}
}

christ = #3
{
	tolerance_heretic = 2 #Love thy neighbor
	tolerance_heathen = 2
	
	allow = { religion = zun_pagan_reformed }
	sprite = 55 #Because no GFX
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			NOT = { num_of_cities = 5 }
		}
		modifier = {
			factor = 2
			NOT = { num_of_cities = 10 }
		}
		modifier = {
			factor = 0.5
			num_of_cities = 30
		}
		modifier = {
			factor = 2
			personality = ai_militarist
		}	
		modifier = {
			factor = 0.5
			personality = ai_capitalist
		}		
	}
}

allah = #4
{
	core_creation = -0.15 #Spread through conquest
	discipline = 0.05
	
	allow = { religion = zun_pagan_reformed }
	sprite = 55 #Because no GFX
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_balanced
		}	
		modifier = {
			factor = 0.5
			personality = ai_colonialist
		}		
	}
}

surya_zun = #5
{
	technology_cost = -0.10
	global_tax_modifier = 0.10
	
	allow = { religion = zun_pagan_reformed }
	sprite = 55 #Because no GFX
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_capitalist
		}	
		modifier = {
			factor = 2
			personality = ai_colonialist
		}	
		modifier = {
			factor = 0.5
			personality = ai_militarist
		}		
	}
}


inti_zun = # 6
{
	global_autonomy = -0.05 #There is only one god
	global_unrest = -2	
	
	allow = { religion = zun_pagan_reformed }
	sprite = 55 #Because no GFX
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_diplomat
		}	
		modifier = {
			factor = 0.5
			personality = ai_militarist
		}		
	}
}
