graphical_culture = easterngfx
color = { 245 200 30 }
revolutionary_colors = { 8 0 16 }
historical_idea_groups = {
	defensive_ideas
	religious_ideas
	offensive_ideas
	spy_ideas
	trade_ideas
	maritime_ideas
	economic_ideas
	diplomatic_ideas
}
historical_units = {
	eastern_carabinier
	eastern_knights
	eastern_medieval_infantry
	eastern_militia
	eastern_skirmisher
	eastern_uhlan
	ottoman_lancer
	ottoman_new_model
	ottoman_nizami_cedid
	ottoman_reformed_janissary
	ottoman_reformed_spahi
	ottoman_sekban
	ottoman_spahi
	ottoman_toprakli_dragoon
	ottoman_toprakli_hit_and_run
	slavic_stradioti
}
monarch_names = {
	"Abbondio #1" = 10
	"Aikaterine #0" = -1
	"Anastasios #0" = 0
	"Anthousa #0" = -1
	"Antiochos #0" = 0
	"Arne #1" = 10
	"Aude #1" = -1
	"Ausala #1" = -1
	"Boethios #0" = 0
	"Bohdan #1" = 10
	"Cecilia #1" = -1
	"David #0" = 0
	"Demetrios #0" = 0
	"Diogenes #0" = 0
	"Eginolf #1" = 10
	"Erlend #1" = 10
	"Gulay #0" = -1
	"Gunnar #1" = 10
	"Hafiz #1" = 10
	"Ioustina #0" = -1
	"Ioustinos #1" = 10
	"Isabel #1" = -1
	"Jacques #1" = 10
	"Jocelyn #1" = 10
	"Kallinikos #0" = 0
	"Kyra #0" = -1
	"Lionel #1" = 10
	"Madu #1" = 10
	"Makarios #0" = 0
	"Martha #1" = -1
	"Nawata #1" = 10
	"Nikolaos #1" = 10
	"Niphon #0" = 0
	"Okhsun #1" = 10
	"Pelagios #0" = 0
	"Philippe #1" = 10
	"Roger #1" = 10
	"Romane #0" = -1
	"Rorgues #1" = 10
	"Sancha #1" = -1
	"Sohvi #1" = -1
	"Yuda #0" = 0
	"�douard #1" = 10
}
leader_names = {
	"Bragadin"
	"Crispo"
	"Doria"
	"Fadrigue"
	"Lusignans"
	"Melissinos"
	"Plethon"
	"Rendis"
	"Saraceno"
	"Tocco"
}
ship_names = {
	"Agios Georgios"
	"Kato Yialla"
	"Lachi"
	"Leivadi"
	"Lempa"
	"Maa"
	"Maurvoll"
	"Pakhyammos"
	"Pomos"
	"Symvoulos"
}
