graphical_culture = westerngfx
color = { 59 126 59 }
revolutionary_colors = { 0 9 0 }
historical_idea_groups = {
	diplomatic_ideas
	economic_ideas
	defensive_ideas
	influence_ideas
	offensive_ideas
	trade_ideas
	administrative_ideas
	quality_ideas
}
historical_units = {
	austrian_grenzer
	austrian_hussar
	austrian_jaeger
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	mixed_order_infantry
	napoleonic_lancers
	napoleonic_square
	open_order_cavalry
	swiss_landsknechten
	western_medieval_infantry
	western_medieval_knights
	western_men_at_arms
}
monarch_names = {
	"Adalbero #1" = 10
	"Adam #1" = 10
	"Adela #1" = -1
	"Alarich #1" = 10
	"Albrecht #3" = 10
	"Amalrich #1" = 10
	"Baldewin #1" = 10
	"Berthold #2" = 10
	"Binhilde #0" = -1
	"Dietwin #1" = 10
	"Emmerich #1" = 10
	"Engelbert #0" = 0
	"Filibert #0" = 0
	"Frida #0" = -1
	"Gerberga #0" = -1
	"Helga #0" = -1
	"Irmeltrud #0" = -1
	"Liutbrand #0" = 0
	"Liutpold #0" = 0
	"Luca #0" = -1
	"Magnus #1" = 10
	"Martin #0" = 0
	"Mechthild #0" = -1
	"Nikolaus #2" = 10
	"Otakar #1" = 10
	"Walpurga #0" = -1
	"Wilhelm #0" = 0
}
leader_names = {
	"Absenger"
	"Allmer"
	"Almer"
	"Altmann"
	"Amtmann"
	"Andreitsch"
	"Arlitz"
	"Fuchs"
	"Herndler"
	"Hierzegger"
	"Kroha"
	"Lanz"
	"Maxa"
	"Messner"
	"Potetz"
	"Ruckenstuhl"
	"Schmidtberger"
	"Steinegger"
	"Teubenbacher"
	"Trenkler"
	"Wergals"
	"Zingl"
}
ship_names = {
	"Bruck"
	"Eisenerz"
	"Graz"
	"Greim"
	"Kapfenberg"
	"Leibnitz"
	"Leoben"
	"M�rztal"
	"Steiermark"
	"Weiz"
}
army_names = {
	"Armee von $PROVINCE$"
}
