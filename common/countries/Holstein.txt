graphical_culture = westerngfx
color = { 220 150 158 }
revolutionary_colors = { 5 0 5 }
historical_idea_groups = {
	trade_ideas
	defensive_ideas
	offensive_ideas
	economic_ideas
	diplomatic_ideas
	aristocracy_ideas
	spy_ideas
	innovativeness_ideas
}
historical_units = {
	dutch_maurician
	mixed_order_infantry
	napoleonic_lancers
	napoleonic_square
	open_order_cavalry
	prussian_frederickian
	prussian_uhlan
	swedish_arme_blanche
	swedish_caroline
	swedish_gallop
	swedish_gustavian
	western_medieval_infantry
	western_medieval_knights
	western_men_at_arms
}
monarch_names = {
	"Anna #2" = -1
	"Bj�rn #1" = 10
	"Dobieslaw #1" = 10
	"Elisabeth #1" = -1
	"Grzegorz #1" = 10
	"Hesso #1" = 10
	"Hjalmar #1" = 10
	"H�rik #1" = 10
	"Izabela #0" = -1
	"Jaromil #1" = 10
	"Jozef #0" = 0
	"Leif #1" = 10
	"Maria #0" = -1
	"Metta #0" = -1
	"Mikkel #1" = 10
	"Milosz #0" = 0
	"Ragnvald #1" = 10
	"Rycheza #0" = -1
	"Sigfred #1" = 10
	"Strasz #1" = 10
	"Svend #1" = 10
	"Swietopelk #0" = 0
	"Uffe #1" = 10
	"Wlodzimierz #1" = 10
	"Zdislav #1" = 10
}
leader_names = {
	"Albert"
	"Augustenborg"
	"G�nther"
	"Holstein"
	"Holstein-Gottorp"
	"Oldenburg"
	"Philip"
	"Rantzau"
	"von Barner"
	"von Dellwigh"
}
ship_names = {
	"Dahme"
	"Grossenbrode"
	"Gr�mtz"
	"Kellenhusen"
	"Kiel"
	"Neustadt"
	"Ostsee"
	"Scharbeutz"
	"Sierksdorf"
	"Wedel"
}
army_names = {
	"Armee von $PROVINCE$"
}
