graphical_culture = muslimgfx
color = { 213 0 39 }
revolutionary_colors = { 1 5 1 }
historical_idea_groups = {
	administrative_ideas
	religious_ideas
	defensive_ideas
	trade_ideas
	economic_ideas
	offensive_ideas
	maritime_ideas
	quantity_ideas
}
historical_units = {
	reformed_steppe_rifles
	steppe_cavalry
	steppe_footmen
	steppe_infantry
	steppe_lancers
	steppe_mounted_raiders
	steppe_musketeers
	steppe_raiders
	steppe_riders
	steppe_rifles
	steppe_uhlans
}
monarch_names = {
	"Aigidia #0" = -1
	"Akakios #0" = 0
	"Alauddin #0" = 0
	"Amar #0" = 0
	"Amice #0" = -1
	"Anthimos #0" = 0
	"Aubrey #0" = 0
	"Aveline #0" = -1
	"Aytekin #0" = 0
	"Balaban #0" = 0
	"Bulend #0" = 0
	"Checheyigen #0" = -1
	"Chrysogone #0" = -1
	"Ertekin #0" = 0
	"Eusebios #0" = 0
	"Furuzan #0" = -1
	"Gul�i�ek #0" = -1
	"Holuikhan #0" = -1
	"Ipekel #0" = -1
	"Kakanvati #0" = -1
	"Khugurchin #0" = -1
	"Lyukha #0" = -1
	"Manrique #0" = 0
	"Mohuai #0" = 0
	"Nangar #0" = 0
	"Nikarete #0" = -1
	"Pantoleon #0" = 0
	"Pokshayka #0" = 0
	"Sarica #0" = -1
	"Savk�lti #0" = -1
	"Sevilay #0" = -1
	"Sherira #0" = 0
	"Sirin #0" = -1
	"Sumru #0" = -1
	"Sybilla #0" = -1
	"Timur #1" = 10
	"Togtekin #0" = 0
	"Tunga #0" = 0
	"Vahide #0" = -1
	"Waleran #0" = 0
	"Yaman #0" = 0
	"Yeldem #0" = -1
	"Zainab Tari #0" = -1
	"Zehra #0" = -1
	"�ilen #0" = -1
}
leader_names = {
	"Abb�s"
	"Ab�"
	"Akbar"
	"Bakr"
	"Jahagir"
	"Jal�l"
	"J�m�"
	"Khalil"
	"Mohsin"
	"Zaman"
	"ud-D�n"
}
ship_names = {
	"Ardabil"
	"Bakhura"
	"Balkh"
	"Baluches"
	"Herat"
	"Ispahan"
	"Kandahar"
	"Kharesm"
	"Kirman"
	"Kish"
	"Lahore"
	"Merv"
	"Nishupur"
	"Ormuz"
	"Rustam"
	"Samarqand"
	"Sebzewar"
	"Shah Rukh"
	"Sikandar"
	"Slurax"
	"Tahriz"
	"Tamerlane"
}
