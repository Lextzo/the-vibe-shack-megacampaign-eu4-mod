graphical_culture = westerngfx
color = { 210 220 175 }
revolutionary_colors = { 1 0 10 }
historical_idea_groups = {
	defensive_ideas
	innovativeness_ideas
	maritime_ideas
	exploration_ideas
	trade_ideas
	diplomatic_ideas
	economic_ideas
	quality_ideas
}
historical_units = {
	ali_bey_reformed_infantry
	mamluk_archer
	mamluk_cavalry_charge
	mamluk_duel
	mamluk_musket_charge
	muslim_dragoon
	muslim_mass_infantry
}
monarch_names = {
	"Abdul-Aziz #0" = 0
	"Adalhard #0" = 0
	"Alberich #0" = 0
	"Aldrich #1" = 10
	"Alois #2" = 10
	"Badis #1" = 10
	"Barbara #1" = -1
	"Berta #0" = -1
	"Buluggin #2" = 10
	"Dietpold #0" = 0
	"Eberhard #1" = 10
	"Gamila #0" = -1
	"Gil #1" = 10
	"Habus #1" = 10
	"Hartmann #0" = 0
	"Hartwig #1" = 10
	"Klara #0" = -1
	"Konstanze #1" = -1
	"Kriemhild #0" = -1
	"Kuno #0" = 0
	"Landolf #0" = 0
	"Layla #0" = -1
	"Martin #1" = 10
	"Oda #0" = -1
	"Osterhild #0" = -1
	"Richenza #1" = -1
	"Rudolf #1" = 10
	"Serhilda #0" = -1
	"Tamim #1" = 10
	"Trude #1" = -1
	"Utman #1" = 10
	"Welf #0" = 0
	"Zawi #1" = 10
}
leader_names = {
	"Ibn Abbad"
	"Ibn Abd Allah"
	"Ibn Abd al-Rahman"
	"Ibn Abi Hafs"
	"Ibn Ali"
	"Ibn Hakan"
	"Ibn Hammud"
	"Ibn Hisam"
	"Ibn Husayn"
	"Ibn Idris"
	"Ibn Ishaq"
	"Ibn Isma'il"
	"Ibn Marwan"
	"Ibn Muhammad"
	"Ibn Nasr"
	"Ibn Qasim"
	"Ibn Sa'd"
	"Ibn Sulayman"
	"Ibn Umar"
	"Ibn Ummay"
	"Ibn Utman"
	"Ibn Ya'far"
	"Ibn Yahy�"
	"Ibn Yusuf"
	"Ibn al-Azafi"
}
ship_names = {
	"Ismail I"
	"Ismail II"
	"Muhammed III"
	"Muhammed IV"
	"Muhammed IX"
	"Muhammed V"
	"Muhammed VI"
	"Muhammed VII"
	"Muhammed VIII"
	"Muhammed X"
	"Muhammed al-Faqih"
	"Muhammed ibn Nasr"
	"Nasr"
	"Yusuf I"
	"Yusuf II"
	"Yusuf III"
	"Yusuf IV"
	"Yusuf V"
}
army_names = {
	"Jaish Al $PROVINCE$"
}
fleet_names = {
	"Alboran Sea Squadron"
	"Balaeric Sea Squadron"
	"Jabal Tariq Squadron"
}
