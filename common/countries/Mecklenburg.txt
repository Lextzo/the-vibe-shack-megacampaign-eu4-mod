graphical_culture = westerngfx
color = { 55 140 143 }
revolutionary_colors = { 13 8 5 }
historical_idea_groups = {
	administrative_ideas
	trade_ideas
	defensive_ideas
	innovativeness_ideas
	diplomatic_ideas
	offensive_ideas
	economic_ideas
	quality_ideas
}
historical_units = {
	austrian_grenzer
	austrian_hussar
	austrian_jaeger
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	mixed_order_infantry
	napoleonic_lancers
	napoleonic_square
	open_order_cavalry
	swiss_landsknechten
	western_medieval_infantry
	western_medieval_knights
	western_men_at_arms
}
monarch_names = {
	"Anna #0" = -1
	"Bodil #0" = -1
	"Boriwoj #1" = 10
	"Bozydar #2" = 10
	"Bretislav #1" = 10
	"Christian #0" = 0
	"Christoffer #0" = 0
	"Dan #0" = 0
	"Esben #1" = 10
	"Eskhakh #0" = 0
	"Fedot #0" = 0
	"Frode #3" = 10
	"Gerdautas #2" = 10
	"Gotfred #0" = 0
	"Grigoriy #0" = 0
	"Gryn #1" = 10
	"Guthskalco #1" = 10
	"Hermann #1" = 10
	"Ingrid #1" = -1
	"Kirsten #0" = -1
	"Klukis #1" = 10
	"Krutoj #1" = 10
	"Krzeslaw #1" = 10
	"Lars #1" = 10
	"Lubomir #1" = 10
	"Magne #0" = 0
	"Margrethe #1" = -1
	"Mikkel #1" = 10
	"Mscislaw #1" = 10
	"Msciwoj #2" = 10
	"Nakon #1" = 10
	"Nomedas #1" = 10
	"Nuraddin #0" = 0
	"Pribigniew #1" = 10
	"Ragna #0" = -1
	"Simeon #0" = 0
	"Sobieslav #1" = 10
	"Swietopelk #1" = 10
	"Tord #0" = 0
	"Vojtech #1" = 10
	"Vseslav #0" = 0
	"Wlodzimierz #1" = 10
}
leader_names = {
	"Busekke"
	"Bu�acker"
	"Gesiter"
	"Gothan"
	"Jacobs"
	"Jauert"
	"Klatt"
	"Laudan"
	"Meincke"
	"Niemann"
	"Panck"
	"Porepp"
	"Reinike"
	"Sass"
	"Schliechting"
	"Schlien"
	"Schuhr"
	"Schultz"
	"Seeland"
	"Steinkopp"
	"Thee�"
	"Wacker"
	"Warncke"
	"Wilcke"
}
ship_names = {
	"F�rst Borwin"
	"F�rst Nikolaus"
	"Herzog Adolf Friedrich"
	"Herzog Albrecht"
	"Herzog Balthasar"
	"Herzog Erich"
	"Herzog Heinrich"
	"Herzog Johann Georg"
	"Herzog Karl"
	"Herzog Magnus"
	"Mekelenborch"
}
army_names = {
	"Armee von $PROVINCE$"
}
