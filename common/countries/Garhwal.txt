graphical_culture = asiangfx
color = { 60 154 135 }
revolutionary_colors = { 0 10 0 }
historical_idea_groups = {
	defensive_ideas
	religious_ideas
	offensive_ideas
	trade_ideas
	quality_ideas
	spy_ideas
	economic_ideas
	diplomatic_ideas
}
historical_units = {
	bhonsle_cavalry
	indian_arquebusier
	indian_footsoldier
	indian_rifle
	mughal_musketeer
	rajput_hill_fighters
	rajput_musketeer
	sikh_hit_and_run
	tipu_sultan_rocket
}
monarch_names = {
	"Asima #0" = -1
	"Dongkar #0" = -1
	"Mangban #0" = 0
	"Ngawang #0" = -1
	"Nytari #0" = 0
	"Phoyongsa #0" = -1
	"Samita #0" = -1
	"Shughur #0" = 0
	"Tridu #0" = 0
	"Trinring #0" = 0
	"Tritog #0" = 0
}
leader_names = {
	"Bais"
	"Bhaal"
	"Bhangalia"
	"Chattar"
	"Chudasama"
	"Jadeja"
	"Katoch"
	"Minha"
	"Pahore"
	"Pakhral"
	"Pundir"
	"Saran"
	"Singh"
}
ship_names = {
	"Beas"
	"Chenab"
	"Devi"
	"Gayatri"
	"Guru"
	"Indus"
	"Jalsena"
	"Jhelum"
	"Lakshmi"
	"Nausena"
	"Nav ka Yudh"
	"Parvati"
	"Radha"
	"Ratri"
	"Ravi"
	"Sagar"
	"Sarasvati"
	"Sita"
	"Sutlej"
}
