graphical_culture = westerngfx
color = { 57 151 142 }
revolutionary_colors = { 1 8 10 }
historical_idea_groups = {
	trade_ideas
	defensive_ideas
	religious_ideas
	economic_ideas
	diplomatic_ideas
	innovativeness_ideas
	spy_ideas
	offensive_ideas
}
historical_units = {
	austrian_grenzer
	austrian_hussar
	austrian_jaeger
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	mixed_order_infantry
	napoleonic_lancers
	napoleonic_square
	open_order_cavalry
	prussian_uhlan
	swiss_landsknechten
	western_medieval_infantry
	western_medieval_knights
	western_men_at_arms
}
monarch_names = {
	"Adelajda #0" = -1
	"Adelinde #0" = -1
	"Anselm #1" = 10
	"Augustin #2" = 10
	"Brunhilde #0" = -1
	"Christopher #3" = 10
	"Dieter #1" = 10
	"Emich #1" = 10
	"Eufemia #0" = -1
	"Frida #0" = -1
	"Friedrich #1" = 10
	"Gerberge #1" = -1
	"Gunzelin #2" = 10
	"Guraya #0" = -1
	"G�raud #1" = 10
	"Hartmann #1" = 10
	"Hedwig #1" = -1
	"Heinrich #1" = 10
	"Hermann #1" = 10
	"Konstanze #0" = -1
	"Maria #1" = -1
	"Sofie #0" = -1
	"Swietoslawa #0" = -1
	"Thomas #0" = 0
	"Trude #0" = -1
}
leader_names = {
	"Adenau"
	"Ansembourg"
	"Bensenraede"
	"Blittersdorf"
	"C�lln"
	"Dammerscheidt"
	"Elmpt"
	"Falkenberg"
	"Gruithausen"
	"Hillensberg"
	"Hohenfeld"
	"Naxleden"
	"Schaluyn"
	"Zebnitsch"
	"von Clausbruch"
	"von Dieblich"
	"von Gelnhausen"
	"von Leyen"
	"von Namedy"
	"von Sch�neck"
}
ship_names = {
	"Deidesheim"
	"Elmstein"
	"Hochspeyer"
	"Kaiserslautern"
	"Landstuhl"
	"Neunkirchen"
	"Neustadt"
	"Rhodt"
	"Winnweiler"
	"Wolfstein"
}
army_names = {
	"Armee von $PROVINCE$"
	"Kurf�rstliche Garde"
}
fleet_names = {
	"Kurf�rstliche Flotte"
}
