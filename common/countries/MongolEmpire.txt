graphical_culture = asiangfx
color = { 130 180 240 }
historical_idea_groups = {
	humanist_ideas
	trade_ideas
	quantity_ideas
	offensive_ideas
	expansion_ideas
	economic_ideas
	defensive_ideas
	administrative_ideas
}
historical_units = {
	reformed_steppe_rifles
	steppe_cavalry
	steppe_footmen
	steppe_infantry
	steppe_lancers
	steppe_mounted_raiders
	steppe_musketeers
	steppe_raiders
	steppe_riders
	steppe_rifles
	steppe_uhlans
}
monarch_names = {
	"Abagha #1" = 10
	"Ajai #0" = 0
	"Ajinai #0" = 0
	"Alan #0" = -1
	"Alchi #2" = 10
	"Altani #0" = -1
	"Asep #0" = 0
	"Ayten #0" = -1
	"Badai #1" = 10
	"Bagaridai #0" = 0
	"Belgutei #0" = 0
	"Bilge #0" = 0
	"Botokhui #0" = -1
	"Bugidai #1" = 10
	"Bugunutei #0" = 0
	"Bulughan #0" = -1
	"Chabi #0" = -1
	"Chagatai #0" = 0
	"Chaghagan #0" = 0
	"Chagur #0" = -1
	"Chakha #0" = -1
	"Checheyigen #0" = -1
	"Chigu #0" = 0
	"Chilagun #1" = 10
	"Chiledu #0" = 0
	"Dayir #1" = 10
	"Doregene #0" = -1
	"Ebegei #0" = -1
	"Ghunan #0" = 0
	"Guchu #0" = 0
	"Guyug #1" = 10
	"Hoelun #0" = -1
	"Holuikhan #0" = -1
	"Ibakha #0" = -1
	"Iturgen #1" = 10
	"Jurchedei #2" = 10
	"Khachi #1" = 10
	"Khadagan #0" = -1
	"Khogaghchin #0" = -1
	"Khutulun #0" = -1
	"Khuyildar #0" = 0
	"Kokochu #2" = 10
	"Mandukhai #0" = -1
	"Megetu #1" = 10
	"Megujin #1" = 10
	"Mongke #0" = 0
	"Mongoljin #0" = -1
	"Nayaga #0" = 0
	"Nomolun #0" = -1
	"Oghul #0" = -1
	"Oljaitu #2" = 10
	"Payk�lti #0" = -1
	"Savintik #0" = -1
	"Shirgugetu #0" = 0
	"Soini #0" = 0
	"Suyiketu #0" = 0
	"Taghai #0" = 0
	"Targhutai #0" = 0
	"Temujin #1" = 10
	"Temulun #0" = -1
	"Temur #0" = 0
	"Tolui #1" = 10
	"T�regene #0" = -1
	"Yesugun #0" = -1
	"Yesui #0" = -1
}
leader_names = {
	"Abaga"
	"Abaganar"
	"Ast"
	"Borjigin"
	"Chagaan"
	"Chakhar"
	"Darqan"
	"Hohhot"
	"Jurim"
	"Karakorum"
	"Khalkha"
	"Kharachin"
	"Khorchin"
	"Noyan"
	"Onriut"
	"Ordos"
	"T�med"
	"Ulanqab"
	"Uriankhai"
	"Wang"
	"Xilingol"
	"Yingchang"
	"Yungshiyebu"
}
ship_names = {
	"Adai"
	"Batu"
	"Bo'orchu"
	"Borokhula"
	"B�ke"
	"Chagaan"
	"Chilaun"
	"Dharmapala"
	"D�rben K�l�'�d"
	"D�rben Noyas"
	"Elbeg"
	"Ghengis Khan"
	"Hohhot"
	"H�leg�"
	"Jebe"
	"Jelme"
	"Jinggim"
	"J�chi"
	"Kamala"
	"Karakorum"
	"Khar Khiruge"
	"Khubilai"
	"Mugali"
	"M�ngke"
	"Qubilai"
	"Shikhikhutug"
	"Sorqan Shira"
	"Subutai"
	"Tolui"
	"Yel� Chucai"
	"Yingchang"
	"�g�dei"
	"�r�g Tem�r"
}
