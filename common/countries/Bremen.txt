graphical_culture = westerngfx
color = { 33 101 137 }
revolutionary_colors = { 5 0 5 }
historical_idea_groups = {
	religious_ideas
	trade_ideas
	defensive_ideas
	economic_ideas
	innovativeness_ideas
	offensive_ideas
	administrative_ideas
	spy_ideas
}
historical_units = {
	austrian_grenzer
	austrian_hussar
	austrian_jaeger
	austrian_tercio
	austrian_white_coat
	dutch_maurician
	mixed_order_infantry
	napoleonic_lancers
	napoleonic_square
	open_order_cavalry
	swiss_landsknechten
	western_medieval_infantry
	western_medieval_knights
	western_men_at_arms
}
monarch_names = {
	"Adalbero #1" = 10
	"Adalbert #1" = 10
	"Adalhard #1" = 10
	"Ademar #1" = 10
	"Adolf #1" = 10
	"Arnold #0" = 0
	"Bernhard #1" = 10
	"Callistus #1" = 10
	"Erich #1" = 10
	"Friedrica #0" = -1
	"Gautselin #0" = 0
	"Gottschalk #1" = 10
	"G�raud #1" = 10
	"Jaspert #1" = 10
	"Landolf #1" = 10
	"Leopold #1" = 10
	"Liutbrand #1" = 10
	"Lothar #0" = 0
	"Lothar-Udo #1" = 10
	"Magnus #1" = 10
	"Simon #0" = 0
	"Sofie #0" = -1
	"Tayyib #0" = 0
	"Zhuan #0" = 0
}
leader_names = {
	"Adler"
	"Alers"
	"Bake"
	"Beckr�ge"
	"Bergius"
	"Berninghausen"
	"Boelken"
	"Bollheim"
	"Brill"
	"Delius"
	"Drewers"
	"Engels"
	"Ficke"
	"Foukhard"
	"Geffken"
	"Gildemeister"
	"Grovermann"
	"Happach"
	"Heimbruch"
	"Hinke"
	"Kindt"
	"Krefting"
	"K�hne"
	"Laporte"
	"L�ning"
	"Meyer"
	"Oelrichs"
	"Ordemann"
	"Reiners"
	"Rohde"
	"Sanders"
	"Schulken"
	"Steinwachs"
	"Stucken"
	"Truernitt"
	"von Bobert"
	"von Cappeln"
	"von Holten"
	"von Horn"
	"von Meinertzhagen"
}
ship_names = {
	"Adler von L�beck"
	"Admiralit�t von Hamburg"
	"Afrikaner"
	"Anna"
	"Arche Noah"
	"Bersteinf�nger"
	"Bracke"
	"Brunte Kuh"
	"Clementia"
	"Colonia"
	"Der Gro�e Adler"
	"Derffinger"
	"Dorothea"
	"Engel"
	"Engel Gabriel"
	"Feuerblase"
	"Fliegender Hirsch"
	"Fortuna"
	"Friede"
	"Fuchs"
	"F�chslein"
	"Gabriel"
	"Gelber L�ve"
	"Goldener L�we"
	"Grafschaft Mark"
	"Gr�ner Drache"
	"Herzogtum Cleve"
	"Hoffning"
	"Jesus von L�beck"
	"Johannesburg"
	"Jonge Tobias"
	"Kurprinz"
	"K�nig David"
	"Leopoldus Primus"
	"Lisa von L�beck"
	"Lybischer Trotz"
	"L�bscher Adler"
	"Makarele"
	"Maria Catharina"
	"Meermann"
	"Meerweib"
	"Mercurius"
	"Morian"
	"Nordischer L�we"
	"Pax"
	"Peter und Paul"
	"Peter von Danzig"
	"Pierelepomp"
	"Prinz Ludwig"
	"Prophet Daniel"
	"Ritter Sankt Georg"
	"Roland"
	"Rommelpot"
	"Rosenfelde"
	"Roter Hirsch"
	"Salamander"
	"Sankt Laurens"
	"Sankt Peter"
	"Schwarzer Rabe"
	"Seemoewe"
	"Seeritter"
	"Sieben Provinzien"
	"Stad Emden"
	"Syrig"
	"Vogel Greif"
	"Wapen von Bremen"
	"Wapen von Hamburg"
	"Wappen von Bremen"
	"Wappen von Cleve"
	"Wasserhund"
	"Wei�er L�we"
	"Wilhelmina Margaretta"
	"Windhund"
	"Wolkens�ule"
	"Zwei Br�der"
}
army_names = {
	"Armee von $PROVINCE$"
	"Erzbischofliche Garde"
}
preferred_religion = protestant
