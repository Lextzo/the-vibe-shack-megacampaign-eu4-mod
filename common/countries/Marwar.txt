graphical_culture = indiangfx
color = { 192 111 179 }
revolutionary_colors = { 6 5 10 }
historical_idea_groups = {
	aristocracy_ideas
	offensive_ideas
	defensive_ideas
	religious_ideas
	trade_ideas
	diplomatic_ideas
	spy_ideas
	economic_ideas
}
historical_units = {
	bhonsle_cavalry
	indian_arquebusier
	indian_footsoldier
	indian_rifle
	mughal_mansabdar
	mughal_musketeer
	rajput_hill_fighters
	reformed_mughal_mansabdar
	sikh_hit_and_run
	tipu_sultan_rocket
}
monarch_names = {
	"Abdurrahman #0" = 0
	"Abdurrashid #1" = 10
	"Ahila #1" = 10
	"Alauddin #0" = 0
	"Anahilla #1" = 10
	"Asvapala #1" = 10
	"Balaprasada #1" = 10
	"Baliraja #1" = 10
	"Banhbina #1" = 10
	"Bauka #1" = 10
	"Bhaadar #1" = 10
	"Bhilladitya #1" = 10
	"Bhima #1" = 10
	"Chanduka #1" = 10
	"Feruzuddin #2" = 10
	"Gang Dev #1" = 10
	"Genhra #1" = 10
	"Harichandra #1" = 10
	"Jayapala #2" = 10
	"Jhota #1" = 10
	"Jinduraja #1" = 10
	"Kahah #0" = 0
	"Kakka #1" = 10
	"Kakuka #1" = 10
	"Kumarapala #2" = 10
	"Lakshamana #1" = 10
	"Madanpal #1" = 10
	"Mahendra #1" = 10
	"Nagabhata #1" = 10
	"Nangar #1" = 10
	"Narabbhata #1" = 10
	"Nattadevi #1" = -1
	"Prithvipala #1" = 10
	"Rajjila #1" = 10
	"Rajkamar 1 #1" = 10
	"Ramadeva #4" = 10
	"Sanjar #1" = 10
	"Shaliwahan #1" = 10
	"Sikandar #0" = 0
	"Siluka #1" = 10
	"Sobhita #1" = 10
	"Sumrah #0" = 0
	"Tata #1" = 10
	"Vigrahapala #1" = 10
	"Vijnayavati #0" = -1
	"Yashovardhana #1" = 10
	"Yuanji #0" = 0
}
leader_names = {
	"Bais"
	"Bhati"
	"Chandela"
	"Chattari"
	"Chudasama"
	"Gauri"
	"Jadeja"
	"Naru"
	"Pakhral"
	"Rathore"
	"Saran"
	"Singh"
	"Solanki"
}
ship_names = {
	"Chambal"
	"Devi"
	"Durga"
	"Gayatri"
	"Ghaggar"
	"Godwar"
	"Jalsena"
	"Kala Jahazi"
	"Lakshmi"
	"Lal Jahazi"
	"Luni"
	"Marwar ka Nav"
	"Merwar ka Nav"
	"Nausena"
	"Nav ka Yudh"
	"Nila Jahazi"
	"Parvati"
	"Radha"
	"Rani"
	"Ratri"
	"Sagar"
	"Sarasvati"
	"Sekhawati"
	"Sita"
}
