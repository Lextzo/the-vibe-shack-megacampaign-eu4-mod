graphical_culture = westerngfx
color = { 153 240 182 }
revolutionary_colors = { 9 5 8 }
historical_idea_groups = {
	administrative_ideas
	plutocracy_ideas
	trade_ideas
	defensive_ideas
	economic_ideas
	expansion_ideas
	quality_ideas
	innovativeness_ideas
}
historical_units = {
	austrian_grenzer
	austrian_hussar
	austrian_jaeger
	austrian_tercio
	austrian_white_coat
	halberd_infantry
	mixed_order_infantry
	napoleonic_lancers
	napoleonic_square
	open_order_cavalry
	swiss_landsknechten
	western_medieval_knights
	western_men_at_arms
}
monarch_names = {
	"Adela #0" = -1
	"Amadeus #1" = 10
	"Archambaud #1" = 10
	"Arnold #1" = 10
	"Berthold #1" = 10
	"Burchard #1" = 10
	"Eberhard #1" = 10
	"Emich #1" = 10
	"Gerlach #1" = 10
	"Gottschalk #1" = 10
	"Gotzelo #1" = 10
	"G�nther #1" = 10
	"Heinrich #1" = 10
	"Humbert #1" = 10
	"Johann #0" = 0
	"Karlmann #1" = 10
	"Kuno #1" = 10
	"K�lm�n #1" = 10
	"Liudolf #1" = 10
	"Lutbert #0" = 0
	"Matthias #1" = 10
	"Meinhard #1" = 10
	"Rudolf #2" = 10
	"Siegmund #1" = 10
	"Sigismund #0" = 0
}
leader_names = {
	"Aebi"
	"Amman"
	"Bischofberger"
	"Blank"
	"Blies"
	"Brugger"
	"Combe"
	"Eugster"
	"Frei"
	"Frisch"
	"Gasenzer"
	"Gr�neisen"
	"Heberlin"
	"Helberingen"
	"Hertzler"
	"Hiltbrand"
	"Honegger"
	"Luz"
	"Miller"
	"Munzinger"
	"Oberholzer"
	"Schwendimann"
	"Sp�rri"
	"Vogt"
	"W�chter"
	"Zumbach"
	"von Baltharsar"
	"von Hasseln"
}
ship_names = {
	"Berne"
	"Dufour"
	"Glarus"
	"Inn"
	"Lucerne"
	"Rhein"
	"Rh�ne"
	"Schwyz"
	"Ticino"
	"Unterwalden"
	"Uri"
	"Wilhelm Tell"
	"Zug"
	"Z�rich"
}
army_names = {
	"Armee von $PROVINCE$"
}
preferred_religion = reformed
