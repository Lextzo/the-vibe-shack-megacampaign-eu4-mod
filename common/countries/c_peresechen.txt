graphical_culture = easterngfx
color = { 225 204 147 }
monarch_names = {
	"Adelheid #0" = -1
	"Caichun #0" = -1
	"Dobronega #0" = -1
	"Gleb #0" = 0
	"Gotthard #0" = 0
	"Ivan #0" = 0
	"Iziaslav #0" = 0
	"Katharina #0" = -1
	"Mabila #0" = -1
	"Matfei #0" = 0
	"Mian #0" = 0
	"Pavel #0" = 0
	"Plaisance #0" = -1
	"Qingyu #0" = -1
	"Svajone #0" = -1
	"Sviatoslav #0" = 0
	"Trifon #0" = 0
	"Trude #0" = -1
}
