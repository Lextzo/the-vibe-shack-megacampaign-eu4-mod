graphical_culture = westerngfx
color = { 74 189 156 }
historical_idea_groups = {
	religious_ideas
	defensive_ideas
	trade_ideas
	expansion_ideas
	offensive_ideas
	maritime_ideas
	economic_ideas
	quality_ideas
}
monarch_names = {
	"Abdul #0" = 0
	"Aldonza #0" = -1
	"Arnfinn #1" = 10
	"Benilde #0" = -1
	"B�rd #0" = 0
	"Cecilia #0" = -1
	"Cecilie #1" = -1
	"Christian #1" = 10
	"Christopher #0" = 0
	"Daniil #1" = 10
	"Einar #0" = 0
	"Feodor #1" = 10
	"Frederikke #0" = -1
	"Gregers #0" = 0
	"Gunnar #0" = 0
	"Gyda #0" = -1
	"Holger #2" = 10
	"Inge #0" = 0
	"Irina #1" = -1
	"Kristian #0" = 0
	"K�re #0" = 0
	"Lev #1" = 10
	"Lodin #0" = 0
	"Margrethe #0" = -1
	"Radoslav #1" = 10
	"Ragna #0" = -1
	"Ragnar #1" = 10
	"Rolf #0" = 0
	"Sigrid #0" = -1
	"Sigurd #1" = 10
	"Ulv #0" = 0
	"Yefimiy #1" = 10
	"�mund #0" = 0
}
