graphical_culture = indiangfx
color = { 205 110 47 }
revolutionary_colors = { 6 0 12 }
historical_idea_groups = {
	defensive_ideas
	religious_ideas
	offensive_ideas
	trade_ideas
	spy_ideas
	economic_ideas
	diplomatic_ideas
	administrative_ideas
}
historical_units = {
	afsharid_reformed
	afsharid_reformed_infantry
	muslim_dragoon
	muslim_mass_infantry
	persian_cavalry_charge
	persian_footsoldier
	persian_rifle
	persian_shamshir
	qizilbash_cavalry
	tofongchis_musketeer
	topchis_artillery
}
monarch_names = {
	"Artashes #0" = 0
	"Bhagi #0" = -1
	"Dahar #0" = -1
	"Hamide #0" = -1
	"Himu #0" = -1
	"Karan #0" = 0
	"Khinrah #0" = 0
	"Lilan #0" = -1
	"Marui #0" = -1
	"Nizamuddin #0" = 0
	"Pahtu #0" = 0
	"Premala #0" = -1
	"Radha #0" = -1
	"Seetha #0" = -1
	"Sumrah #0" = 0
	"Taghlak #0" = 0
}
leader_names = {
	"Ali Sherwan"
	"Barzani"
	"Bukhti"
	"Ferozkohi"
	"Feyli"
	"Gewirk"
	"Hadhabani"
	"Herki"
	"Jaff"
	"Jalilavand"
	"Mangur"
	"Rekani"
	"Sanj�bi"
	"Sheylanli"
	"Zafaranlu"
	"Zarrinkafsh"
}
ship_names = {
	"Abathur"
	"Adamn Kasia"
	"Aesma Daeva"
	"Agas"
	"Ahaka"
	"Ahriman"
	"Ahura"
	"Ahura Mazda"
	"Ahurani"
	"Airyaman"
	"Aka Manah"
	"Aladdin"
	"Allatum"
	"Amashaspan"
	"Ameratat"
	"Amesha Spentas"
	"Anahita"
	"Angra Mainyu"
	"Anjuman"
	"Apam-natat"
	"Apaosa"
	"Aredvi"
	"Arishtat"
	"Armaiti"
	"Arsaces"
	"Asha vahista"
	"Asman"
	"Asto Vidatu"
	"Astvat-Ereta"
	"Atar"
	"Azi Dahaka"
	"Baga"
	"Bahram"
	"Burijas"
	"Bushyasta"
	"Buyasta"
	"Camros"
	"Daena"
	"Daevas"
	"Dahhak"
	"Dena"
	"Dev"
	"Diwe"
	"Drug"
	"Drvaspa"
	"Frashegird"
	"Fravashis"
	"Gandarewa"
	"Gao-kerena"
	"Gayomard"
	"Gayomart"
	"Geus-Tasan"
	"Geus-Urvan"
	"Haoma"
	"Haurvatat"
	"Humay"
	"Hvar"
	"Hvarekhshaeta"
	"Indar"
	"Indra"
	"Izha"
	"Jamshid"
	"Jeh"
	"Karshipta"
	"Kavi"
	"Khara"
	"Khshathra vairya"
	"Kundrav"
	"Mah"
	"Mahre"
	"Mahrianag"
	"Manu"
	"Manuchihir"
	"Mao"
	"Mashyane"
	"Mashye"
	"Menog"
	"Mithra"
	"Nairyosangha"
	"Nanghaithya"
	"Neriosang"
	"Ormazd"
	"Peris"
	"Peshdadians"
	"Rapithwin"
	"Rashnu"
	"Rustam"
	"Saurva"
	"Simurgh"
	"Spenta Mainyu"
	"Sraosa"
	"Srosh"
	"Tawrich"
	"Thunaupa"
	"Tistrya"
	"Tushnamatay"
	"Vanant"
	"Vata"
	"Verethragna"
	"Vohu Manah"
	"Vouruskasha"
	"Yam"
	"Yasht"
	"Yazata"
	"Yezidi"
	"Yima"
	"Zal"
	"Zam"
	"Zam-Armatay"
	"Zarathustra"
	"Zarich"
	"Zend-Avesta"
	"Zurvan"
}
army_names = {
	"$PROVINCE$"
	"Artesa"
}
fleet_names = {
	"$PROVINCE$ Donanmas�"
}
