graphical_culture = asiangfx
color = { 40 75 175 }
revolutionary_colors = { 4 0 13 }
historical_idea_groups = {
	defensive_ideas
	offensive_ideas
	trade_ideas
	spy_ideas
	diplomatic_ideas
	economic_ideas
	quality_ideas
	quantity_ideas
}
historical_units = {
	afsharid_reformed
	afsharid_reformed_infantry
	muslim_dragoon
	muslim_mass_infantry
	persian_cavalry_charge
	persian_footsoldier
	persian_rifle
	persian_shamshir
	qizilbash_cavalry
	shaybani
	tofongchis_musketeer
	topchis_artillery
}
monarch_names = {
	"Alutar #1" = 10
	"Badai #1" = 10
	"Bar�s #0" = 0
	"Bugidai #1" = 10
	"Bugunutei #1" = 10
	"Buqa #1" = 10
	"Buyruq #1" = 10
	"Chagatai #1" = 10
	"Dagun #1" = 10
	"Dodai #2" = 10
	"Elig #0" = 0
	"Esfandiar #1" = 10
	"Horkhudagh #1" = 10
	"Hulegu #1" = 10
	"Ilterish #2" = 10
	"Inalchi #2" = 10
	"Jurchedei #2" = 10
	"J�k�ntirig #0" = 0
	"Ke�ig #1" = 10
	"Khachi #1" = 10
	"Khudu #1" = 10
	"Khuyildar #1" = 10
	"Kokochu #1" = 10
	"Mahmud #1" = 10
	"Morteza #2" = 10
	"Nayaga #1" = 10
	"Oljaitu #1" = 10
	"Pai #1" = 10
	"Qumash #0" = 0
	"Salim #1" = 10
	"Tardush #1" = 10
	"Temur #1" = 10
	"Toghoril #1" = 10
	"Tolui #1" = 10
	"Uru� #1" = 10
	"�u�ib�ri #1" = 10
}
leader_names = {
	"Alim"
	"Bachi"
	"Bahadur"
	"Erdeni"
	"Khudayar"
	"Malla"
	"Murad"
	"Quli"
	"Tura"
}
ship_names = {
	"Alajskij Chrebet"
	"Amu Darja"
	"Besharik"
	"Fergana"
	"Khamza"
	"Khavakend"
	"Kuva"
	"Kuvasay"
	"Margilan"
	"Nar"
	"Rishdan"
	"Syr Darja"
	"Uzeravsan"
}
