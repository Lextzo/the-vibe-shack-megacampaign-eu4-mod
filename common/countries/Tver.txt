graphical_culture = easterngfx
color = { 136 158 183 }
revolutionary_colors = { 8 5 8 }
historical_idea_groups = {
	trade_ideas
	defensive_ideas
	offensive_ideas
	spy_ideas
	economic_ideas
	diplomatic_ideas
	innovativeness_ideas
	quality_ideas
}
historical_units = {
	bardiche_infantry
	druzhina_cavalry
	eastern_carabinier
	eastern_militia
	eastern_skirmisher
	eastern_uhlan
	muscovite_caracolle
	muscovite_cossack
	muscovite_musketeer
	muscovite_soldaty
	russian_cuirassier
	russian_green_coat
	russian_lancer
	russian_mass
	russian_petrine
	slavic_stradioti
}
monarch_names = {
	"Afanasiy #1" = 10
	"Azuolas #1" = 10
	"Birger #0" = 0
	"Bojan #1" = 10
	"Briachislav #1" = 10
	"Daumantas #1" = 10
	"Dmitriy #1" = 10
	"Dorothea #0" = -1
	"Etrek #1" = 10
	"Feodor #1" = 10
	"Gavriil #0" = 0
	"Gremislava #0" = -1
	"Gunduz #0" = -1
	"Gunhild #0" = -1
	"Hongmin #0" = 0
	"Igor #2" = 10
	"Iliya #1" = 10
	"Ivan #0" = 0
	"Ivomai #0" = 0
	"Karin #0" = -1
	"Klara #0" = -1
	"Kobyak #1" = 10
	"Linda #0" = -1
	"Nikita #1" = 10
	"Pavel #1" = 10
	"Pulad #1" = 10
	"Radoslav #1" = 10
	"Rikissa #0" = -1
	"Roman #1" = 10
	"Roze #1" = -1
	"Sigrid #0" = -1
	"Sudislav #1" = 10
	"Svetozar #0" = 0
	"Sviatoslava #1" = -1
	"Viacheslava #0" = -1
	"Vladimir #0" = 0
	"Volodar #1" = 10
	"Yefrosinia #1" = -1
	"Yeremey #1" = 10
	"Zhavoronok #1" = 10
}
leader_names = {
	"Andomsky"
	"Beloselsky"
	"Chernyatinsky"
	"Dorogobushsky"
	"Dyabrinsky"
	"Kargolomsky"
	"Kashinsky"
	"Kemsky"
	"Kholmsky"
	"Mikulinsky"
	"Sheleshpansky"
	"Sugorsky"
	"Telyatevsky"
	"Ukhtomsky"
	"Vadbolsky"
}
ship_names = {
	"Brosno"
	"Dmitry"
	"Kashin"
	"Kholmsky"
	"Mikhail"
	"Rzev"
	"Startsa"
	"Toropets"
	"Torzhok"
	"Tvertsa"
	"Valdai"
	"Volga"
	"Vyshniy Voloche"
	"Yaroslavich"
	"Zubtsov"
}
