graphical_culture = westerngfx
color = { 140 60 60 }
monarch_names = {
	"Adalbero #0" = 0
	"Aishno #1" = 10
	"Borut #1" = 10
	"Burchard #1" = 10
	"Daira #1" = -1
	"Eeri #1" = 10
	"Ekkehard #0" = 0
	"Ferdinand #0" = 0
	"Folkhard #0" = 0
	"Folkmar #0" = 0
	"Galene #1" = -1
	"Gintautas #1" = 10
	"Ginvilas #1" = 10
	"Giselbert #0" = 0
	"Gotthard #0" = 0
	"Gunhilda #0" = -1
	"Hartmann #0" = 0
	"Heameel #1" = 10
	"Hongwu #0" = 0
	"Hurmas #3" = 10
	"Ida #0" = -1
	"Janis #1" = 10
	"J�rgen #1" = 10
	"Lembit #1" = 10
	"Liudas #1" = 10
	"Magnus #1" = 10
	"Manegold #0" = 0
	"Markward #1" = 10
	"Mechthild #0" = -1
	"Siegfried #1" = 10
	"Sotvaras #2" = 10
	"Svitrigaila #1" = 10
	"Tarvet #1" = 10
	"Tautvila #1" = 10
	"T�nn #1" = 10
	"Uku #2" = 10
	"Vilit�iv #3" = 10
	"Walram #1" = 10
}
