graphical_culture = easterngfx
color = { 117 112 131 }
revolutionary_colors = { 2 8 2 }
historical_idea_groups = {
	defensive_ideas
	offensive_ideas
	trade_ideas
	spy_ideas
	economic_ideas
	diplomatic_ideas
	religious_ideas
	innovativeness_ideas
}
historical_units = {
	bardiche_infantry
	druzhina_cavalry
	eastern_carabinier
	eastern_militia
	eastern_skirmisher
	eastern_uhlan
	muscovite_caracolle
	muscovite_cossack
	muscovite_musketeer
	muscovite_soldaty
	russian_cuirassier
	russian_green_coat
	russian_lancer
	russian_mass
	russian_petrine
	slavic_stradioti
}
monarch_names = {
	"Alexandr #1" = 10
	"Balgor #1" = 10
	"Boleslava #1" = -1
	"Boris #1" = 10
	"Dmitriy #0" = 0
	"Dobroslava #0" = -1
	"Dobrynia #0" = 0
	"Dodai #0" = 0
	"Fedot #3" = 10
	"Feodor #1" = 10
	"Fevronia #0" = -1
	"Gerard #1" = 10
	"Gorislava #1" = -1
	"Kortan #1" = 10
	"Malfrida #0" = -1
	"Sevilay #0" = -1
	"Stanislav #0" = 0
	"Stepan #0" = 0
	"Sudislav #1" = 10
	"Svetozar #1" = 10
	"Sviatopolk #0" = 0
	"Tyrach #1" = 10
	"Verkhoslava #0" = -1
	"Viacheslav #0" = 0
	"Vladislav #1" = 10
	"Xenia #1" = -1
	"Yuriy #0" = 0
	"Zoe #0" = -1
}
leader_names = {
	"Andrukhovych"
	"Cheremshyna"
	"Dukhnovych"
	"Holovatskyi"
	"Hutsalo"
	"Khvylovy"
	"Kobylyanska"
	"Korniychuk"
	"Kostomarov"
	"Kulish"
	"Kvitka"
	"Marinina"
	"Palahniuk"
	"Pokalchuk"
	"Shashkevych"
	"Stefanyk"
	"Stelmakh"
	"Tobilevich"
	"Vovchok"
	"Vyshnia"
	"Zabara"
}
ship_names = {
	"Akimivka"
	"Dnieper"
	"Energodar"
	"Guliajpole"
	"Khortytsia"
	"Melitopol"
	"Mikhaylovka"
	"Orekhov"
	"Pologi"
	"Tokmak"
	"Vesele"
	"Viliansk"
	"Zaporizhia Sich"
	"Zaporozhian Voisko"
}
